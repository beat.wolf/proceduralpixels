package lib;

import java.util.Arrays;

public class Bitmap implements Cloneable{

	public int [][] pixels;
	
	public Bitmap(int width, int heigth){
		pixels = new int[width][heigth];
	}
	
	public int getHeight(){
		return pixels[0].length;
	}
	
	public int getWidth(){
		return pixels.length;
	}
	
	public Bitmap clone(){
	    Bitmap clone = new Bitmap(getWidth(), getHeight());
	    for(int x = 0; x < pixels.length; x++){
	        System.arraycopy(pixels[x], 0, clone.pixels[x], 0, pixels[x].length);
	    }
	    
	    return clone;
	}
}
