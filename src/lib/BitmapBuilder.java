package lib;

import lib.utilities.BitmapTransformation;


public class BitmapBuilder {

	public Bitmap modifyBitmap(Bitmap bitmap, BitmapTransformation ... bitmapTransformations){
		
		for(BitmapTransformation transformation: bitmapTransformations){
			transformation.transform(bitmap);
		}
		
		return bitmap;
	}
	
}
