package lib;

public interface BitmapGenerator {

    public Bitmap generate();
    
}
