package lib;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;


public class PixelTools {

	public static Color overlayColors(Color base, Color top){
		double factorNew = top.getAlpha() / 255.;
		double factorOriginal = 1 -  factorNew;
		
		int r = (int)(base.getRed() * factorOriginal + top.getRed() * factorNew);
		int g = (int)(base.getGreen() * factorOriginal + top.getGreen() * factorNew);
		int b = (int)(base.getBlue() * factorOriginal + top.getBlue() * factorNew);
		
		return new Color(r,g,b, base.getAlpha());
	}
	
	private static int average(int a, int b, double factorB){
	    double factorA = 1 - factorB;
	    return (int)(a*factorA + b*factorB);
	}
	
	public static Color mergeColors(Color a, Color b, double factorB){
	    int red = average(a.getRed(), b.getRed(), factorB);
	    int green = average(a.getGreen() , b.getGreen(), factorB);
	    int blue = average(a.getBlue() , b.getBlue(), factorB);
	    int alpha = average(a.getAlpha() , b.getAlpha(), factorB);
	    
	    return new Color(red, green, blue, alpha);
	}
	
	public static BufferedImage createImageBuffer(Bitmap bitmap){
		BufferedImage buffer = new BufferedImage(bitmap.getWidth(), bitmap.getHeight(), BufferedImage.TYPE_INT_ARGB);
		
		for(int x = 0; x < bitmap.getWidth(); x++){
			for(int y = 0; y < bitmap.getHeight(); y++){
				buffer.setRGB(x, y, bitmap.pixels[x][y]);
			}
		}
		
		return buffer;
	}
	
	public static Bitmap createBitmapFromBuffer(BufferedImage image){
	    Bitmap bitmap = new Bitmap(image.getWidth(), image.getHeight());
        
        for(int x = 0; x < image.getWidth(); x++){
            for(int y = 0; y < image.getHeight(); y++){
                bitmap.pixels[x][y] = image.getRGB(x, y);
            }
        }
        
        return bitmap;
	}
	
	public static Bitmap resizeBitmap(Bitmap bitmap, int width, int heigth){
	    BufferedImage image = createImageBuffer(bitmap);
	    
	    BufferedImage dbi = new BufferedImage(width, heigth, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g = dbi.createGraphics();
        
        double wRatio = width / (double)image.getWidth();
        double hRatio = heigth / (double)image.getHeight();
        
        AffineTransform at = AffineTransform.getScaleInstance(wRatio, hRatio);
        g.drawRenderedImage(image, at);
        
	    return createBitmapFromBuffer(dbi);
	}
	
	public static void fillRect(Bitmap bitmap, int xPos, int yPos, int w, int h, Color color){
	    for(int x = xPos; x < xPos+w && x < bitmap.getWidth(); x++){
            for(int y = yPos; y < yPos+h && y < bitmap.getHeight(); y++){
                bitmap.pixels[x][y] = color.getRGB();
            }
	    }
	}
	
}
