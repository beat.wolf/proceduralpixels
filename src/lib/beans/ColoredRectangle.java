package lib.beans;

public class ColoredRectangle extends Rectangle {

	private final PColor color;

	public ColoredRectangle(Point topLeft, Point bottomRight, final PColor color) {
		super(topLeft, bottomRight);
		this.color = color;
	}
	
	public PColor getColor(){
		return color;
	}
}
