package lib.beans;

import java.awt.Color;

public class PColor {

    private int r, g, b, a;
    
    private PColor(int r, int g, int b, int a){
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = a;
    }
    
    public static PColor create(){
        return createGray(255);
    }
    
    public static PColor createGray(int level){
        return createColor(level, level, level, 255);
    }
    
    public static PColor createColor(int r, int g, int b){
        return createColor(r,g,b,255);
    }
    
    public static PColor createColor(int r, int g, int b, int a){
        return new PColor(r, g, b, a);
    }
    
    public static PColor loadColor(int colorCode){
        Color color = new Color(colorCode, true);
        return createColor(color);
    }
    
    public static PColor createColor(Color color){
    	return new PColor(color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());
    }
    
    public int getRGBA(){
        return new Color(r, g, b, a).getRGB();
    }
    
    public Color getJavaColor(){
        return new Color(r, g, b, a);
    }
    
    public void setAlpha(int alpha){
        this.a = alpha;
    }
    
    public int getAlpha(){
        return a;
    }
    
    public void setRed(int r){
        this.r = r;
    }
    
    public int getRed(){
        return r;
    }
    
    public void setGreen(int g){
        this.g = g;
    }
    
    public int getGreen(){
        return g;
    }
    
    public void setBlue(int b){
        this.b = b;
    }
    
    public int getBlue(){
        return b;
    }
    
    public String toString(){
        return r+" "+g+" "+b+" "+a;
    }
}
