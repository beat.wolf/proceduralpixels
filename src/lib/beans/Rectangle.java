package lib.beans;

public class Rectangle {

	private final Point topLeft;
	private final Point bottomRight;
	
	public Rectangle(Point topLeft, Point bottomRight){
		this.topLeft = topLeft;
		this.bottomRight = bottomRight;
	}
	
	public Point getTopLeft(){
		return topLeft;
	}
	
	public Point getBottomRight(){
		return bottomRight;
	}
	
	public Point getCenter() {
		return new Point((topLeft.getX() + bottomRight.getX() / 2), (topLeft.getY() + bottomRight.getY() / 2));
	}
}
