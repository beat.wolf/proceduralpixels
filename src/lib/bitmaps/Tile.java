package lib.bitmaps;

import java.awt.Color;

import lib.Bitmap;
import lib.BitmapGenerator;
import lib.PixelTools;

public class Tile implements BitmapGenerator{

    private int w, h;
    
    public Tile(int w, int h){
        this.w = w;
        this.h = h;
    }
    
    protected int getWidth(){
        return w;
    }
    
    protected int getHeight(){
        return h;
    }
    
    @Override
    public Bitmap generate() {
        Bitmap bitmap = new Bitmap(getWidth(), getHeight());
        PixelTools.fillRect(bitmap, 0 ,0 , getWidth(), getHeight(), new Color(0,0,0,0));
        return bitmap;
    }
}
