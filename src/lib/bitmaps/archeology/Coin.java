package lib.bitmaps.archeology;

import java.awt.Color;

import lib.Bitmap;
import lib.BitmapBuilder;
import lib.BitmapGenerator;
import lib.utilities.filter.ColorCutter;
import lib.utilities.filter.Colorizer;
import lib.utilities.generators.geometry.CircleGenerator;
import lib.utilities.generators.geometry.ColorGenerator;
import lib.utilities.random.RandomNumber;
import lib.utilities.transformers.Overlay;

public class Coin implements BitmapGenerator{

    private int w, h;
    public Coin(int width, int height){
        w = width;
        h = height;
    }
    
    @Override
    public Bitmap generate() {
        BitmapBuilder builder = new BitmapBuilder();

        double size = RandomNumber.getNumber(0.7, 0.8);

        Bitmap base = builder.modifyBitmap(new CircleGenerator(size).generate(w, h), new Colorizer(new Color(0xc49725), true));
        
        double innerSize = RandomNumber.getNumber(0.55, size * 0.9);
        
        Bitmap base2 = builder.modifyBitmap(new CircleGenerator(innerSize).generate(w, h), new Colorizer(new Color(0xdbb923), true));
        
        base = builder.modifyBitmap(base, new Overlay(base2, 1));
        
        if(Math.random() < 0.4) {
        	double cutoutSize = RandomNumber.getNumber(0.1, innerSize * 0.6);
            
            builder.modifyBitmap(base, new Overlay(new CircleGenerator(cutoutSize).generate(w,h), 1), new ColorCutter(Color.black, Color.black));
        }else {
        	double cutoutSize = RandomNumber.getNumber(0.1, innerSize * 0.6);
            
            builder.modifyBitmap(base, new Overlay(new ColorGenerator(Color.black).generate((int)(w*cutoutSize), (int)(h*cutoutSize)), 1, true), new ColorCutter(Color.black, Color.black));
        }
        
        
        
        return base;
    }

}
