package lib.bitmaps.heightmap;

import java.awt.Color;
import java.util.Random;

import lib.Bitmap;
import lib.BitmapBuilder;
import lib.BitmapGenerator;
import lib.beans.PColor;
import lib.beans.Point;
import lib.utilities.Generator;
import lib.utilities.Gradient;
import lib.utilities.filter.Blur;
import lib.utilities.filter.GradientColorizer;
import lib.utilities.generators.RandomNoise;

public class BasicHeightmap implements BitmapGenerator{

    private int w, h;
    
    public BasicHeightmap(int w, int h){
        this.w = w;
        this.h = h;
    }
    
    private void rain(Bitmap bitmap, double droplets){
        int drops = (int)(bitmap.getHeight() * bitmap.getWidth() * droplets);
        
        Random rand = new Random();
        
        for(int i = 0; i < drops; i++){
            int x = rand.nextInt(bitmap.getWidth());
            int y = rand.nextInt(bitmap.getWidth());
            
            PColor color = PColor.loadColor(bitmap.pixels[x][y]);
            
            color.setBlue(Math.min(255, color.getBlue() + 255));
            
            bitmap.pixels[x][y] = color.getRGBA();
        }
    }
    
    private void convertToGrayscale(Bitmap bitmap){
        for(int x = 0; x < bitmap.getWidth(); x++){
            for(int y = 0; y < bitmap.getHeight(); y++){
                PColor color = PColor.loadColor(bitmap.pixels[x][y]);
                PColor newColor = PColor.createGray(color.getRed());
                bitmap.pixels[x][y] = newColor.getRGBA();
                
                /*color.setGreen(0);
                color.setBlue(0);
                color.setRed(0);
                bitmap.pixels[x][y] = color.getRGBA();*/
            }
        }
    }
    
    private void convertToRed(Bitmap bitmap){
        for(int x = 0; x < bitmap.getWidth(); x++){
            for(int y = 0; y < bitmap.getHeight(); y++){
                PColor color = PColor.loadColor(bitmap.pixels[x][y]);
                color.setBlue(0);
                color.setGreen(0);
                
                bitmap.pixels[x][y] = color.getRGBA();
            }
        }
    }
    
    private Point getLowest(Bitmap bitmap, int x, int y){
        
        Point lowest = new Point(x, y);
        int low = Integer.MAX_VALUE;
        
        int myX = Math.max(0, x-1);
        for(; myX < bitmap.getWidth() && myX <= x+1 ; myX++){
            int myY = Math.max(0, y-1);
            for(; myY < bitmap.getHeight() && myY <= y+1; myY++){
                PColor color = PColor.loadColor(bitmap.pixels[myX][myY]);
                
                if(color.getRed() + color.getBlue() < low){
                    low = color.getRed() + color.getBlue();
                    lowest = new Point(myX, myY);
                }
            }
        }
        
        return lowest;
    }
        
    private void flowWater(Bitmap bitmap){
        Bitmap bitmapNew = new Bitmap(bitmap.getWidth(), bitmap.getHeight());
        
        for(int x = 0; x < bitmap.getWidth(); x++){
            for(int y = 0; y < bitmap.getHeight(); y++){
                PColor color = PColor.loadColor(bitmap.pixels[x][y]);
                
                if(color.getBlue() > 0){
                    Point lowest = getLowest(bitmap, x, y);
                    if(!(lowest.getX() == x && lowest.getY() == y)){
                        PColor colorLowest = PColor.loadColor(bitmap.pixels[lowest.getX()][lowest.getY()]);
                        
                        int water = colorLowest.getBlue() + color.getBlue();
                        
                        /*int difference = color.getRed() - colorLowest.getRed();
                        
                        int lowerWater = Math.min(difference, water);
                        water -= lowerWater;
                        
                        lowerWater = Math.min(255,  lowerWater + water / 2);
                        
                        color.setBlue(water / 2);
                        colorLowest.setBlue(lowerWater);
                        //colorLowest.setRed(Math.min(255, colorLowest.getRed() + 1));
                        color.setRed(Math.max(0, color.getRed() - 1));
                        color.setGreen(Math.min(255, color.getGreen() + 1));*/
                        
                        color.setBlue(0);
                        colorLowest.setBlue(Math.min(255, water));
                        color.setGreen(Math.min(255, color.getGreen() + 40));
                        color.setRed(Math.max(0, color.getRed() - 10));
                        
                        bitmapNew.pixels[x][y] = color.getRGBA();
                        bitmapNew.pixels[lowest.getX()][lowest.getY()] = colorLowest.getRGBA();
                    }else{
                        bitmapNew.pixels[x][y] = color.getRGBA();
                    }
                }else{
                    bitmapNew.pixels[x][y] = color.getRGBA();
                }
            }
        }
        
        bitmap.pixels = bitmapNew.pixels;
    }
    
    private void printMap(Bitmap bitmap){
        for(int x = 0; x < bitmap.getWidth(); x++){
            for(int y = 0; y < bitmap.getHeight(); y++){
                PColor color = PColor.loadColor(bitmap.pixels[x][y]);
                System.out.println(color);
            }
        }
    }
    
    private void runSimulation(Bitmap bitmap, int steps){
        rain(bitmap, 0.05);
        
        for(int i = 0; i < steps; i++){
            flowWater(bitmap);
        }
    }
    
    @Override
    public Bitmap generate() {
        BitmapBuilder builder = new BitmapBuilder();
        
        Generator noise = new RandomNoise(true);
        
        Bitmap bitmap = builder.modifyBitmap(noise.generate(w, h),
                new Blur());
        
        convertToRed(bitmap);        
        
        for(int i = 0; i < 100; i++){
            runSimulation(bitmap, 20);
        }
        //printMap(bitmap);
        
        //Make ready for release
        convertToGrayscale(bitmap);
        
        bitmap = builder.modifyBitmap(bitmap,
                new GradientColorizer(new Gradient(0, 255, Color.blue, Color.green)));
        
        return bitmap;
    }

}
