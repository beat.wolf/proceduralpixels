package lib.bitmaps.minecraft;

import java.awt.Color;

import lib.Bitmap;
import lib.PixelTools;
import lib.beans.PColor;
import lib.bitmaps.Tile;
import lib.bitmaps.minecraft.tiles.ground.Dirt;
import lib.bitmaps.minecraft.tiles.ground.DirtGrass;
import lib.bitmaps.minecraft.tiles.ground.Grass;
import lib.bitmaps.minecraft.tiles.ground.Gravel;
import lib.bitmaps.minecraft.tiles.ground.Sand;
import lib.bitmaps.minecraft.tiles.ground.Sand2;
import lib.bitmaps.minecraft.tiles.misc.BlueThing;
import lib.bitmaps.minecraft.tiles.misc.Wool;
import lib.bitmaps.minecraft.tiles.stones.Bedrock;
import lib.bitmaps.minecraft.tiles.stones.Bluestone;
import lib.bitmaps.minecraft.tiles.stones.Coal;
import lib.bitmaps.minecraft.tiles.stones.Crystal;
import lib.bitmaps.minecraft.tiles.stones.Gold;
import lib.bitmaps.minecraft.tiles.stones.Iron;
import lib.bitmaps.minecraft.tiles.stones.Redstone;
import lib.bitmaps.minecraft.tiles.stones.Stone;
import lib.bitmaps.minecraft.tiles.woods.Planks;
import lib.bitmaps.minecraft.tiles.woods.Wood;
import lib.utilities.special.BitmapInserter;

public class MinecraftTiles extends Tile{

	private final int w;
	private final int h;
	
	public MinecraftTiles(int width, int height) {
		super(256, 256);
		this.w = width;
		this.h = height;
    }
	
    public MinecraftTiles() {
        this(16, 16);
    }

    @Override
    public Bitmap generate() {
        Bitmap terrain = new Bitmap(getWidth(), getHeight());
        
        PixelTools.fillRect(terrain, 0, 0, getWidth(), getHeight(), Color.white);
        
        Tile[][] tiles = new Tile[][]{
                {new Grass(w,h), new Stone(w,h), new Dirt(w,h), new DirtGrass(w,h), new Planks(w, h)},
                {new Tile(w,h), new Bedrock(w,h), new Sand(w,h), new Gravel(w,h)
                ,new Wood(w,h)},
                {new Gold(w,h), new Iron(w,h), new Coal(w,h)},
                {new Tile(w,h), new Tile(w,h), new Crystal(w, h), new Redstone(w, h)},
                {new Wool(w,h)},
                {new Tile(w,h)},
                {new Tile(w,h)},
                {new Tile(w,h), new Wool(w,h, PColor.createGray(0)), new Wool(w,h, PColor.createGray(50))},
                {new Tile(w,h), new Wool(w,h, PColor.createColor(150, 0, 0)), new Wool(w,h, PColor.createColor(255, 100, 100))},
                {new BlueThing(w, h), new Wool(w,h, PColor.createColor(0, 150, 0)), new Wool(w,h, PColor.createColor(100, 255, 100))},
                {new Bluestone(w, h), new Wool(w,h, PColor.createColor(90, 50, 20)), new Wool(w,h, PColor.createColor(220, 220, 50))},
                {new Sand2(w, h), new Wool(w,h, PColor.createColor(0, 0, 150)), new Wool(w,h, PColor.createColor(100, 100, 255))},
        };
        
        tileArray(terrain, tiles);
        
        /*tile(terrain, new Grass(16,16), new Stone(16,16), new Dirt(16, 16),
                new DirtGrass(16, 16));*/
                
        return terrain;
    }

    private void tileArray(Bitmap base, Tile[][] tiles){
        int x = 0;
        int y = 0;
        
        BitmapInserter inserter = new BitmapInserter(true);
        
        for(int tX = 0; tX < tiles.length; tX++){
            x = 0;
            int lastHeight = 0;
            for(int tY = 0; tY < tiles[tX].length; tY++){
                Tile tile = tiles[tX][tY];
                Bitmap bitmap = tile.generate();
                
                inserter.insert(base, bitmap, x, y);
                x += bitmap.getWidth();
                lastHeight = bitmap.getHeight();
            }
            y += lastHeight;
        }
    }
    
    private void tile(Bitmap base, Tile ... tiles){
        int x = 0;
        int y = 0;
        
        BitmapInserter inserter = new BitmapInserter(true);
        
        for(Tile tile: tiles){
            Bitmap bitmap = tile.generate();
            inserter.insert(base, bitmap, x, y);
            x += bitmap.getWidth();
            if(x >= base.getWidth()){
                x = 0;
                y += bitmap.getHeight();
            }
        }
    }
    
}
