package lib.bitmaps.minecraft.tiles.ground;

import java.awt.Color;

import lib.Bitmap;
import lib.BitmapBuilder;
import lib.BitmapGenerator;
import lib.bitmaps.Tile;
import lib.utilities.Generator;
import lib.utilities.Gradient;
import lib.utilities.filter.GradientColorizer;
import lib.utilities.generators.RandomNoise;
import lib.utilities.transformers.Overlay;

public class Dirt extends Tile{
    
    public Dirt(int w, int h) {
        super(w, h);
    }

    @Override
    public Bitmap generate() {
        BitmapBuilder builder = new BitmapBuilder();

        Generator generator = new RandomNoise(true);
        
        Bitmap dirt = builder.modifyBitmap(generator.generate(getWidth(), getHeight()),
                new GradientColorizer(
                        new Gradient(20, 240, new Color(185, 133, 92), new Color(89, 61, 41))));  
        
        Bitmap sprinkle = builder.modifyBitmap(generator.generate(getWidth(), getHeight()),
                new GradientColorizer(
                        new Gradient(245, 245, new Color(0,0,0,0), new Color(135, 135, 135))));  
        
        return builder.modifyBitmap(dirt, new Overlay(sprinkle, 1));
    }

}
