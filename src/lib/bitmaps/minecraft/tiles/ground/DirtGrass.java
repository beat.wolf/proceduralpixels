package lib.bitmaps.minecraft.tiles.ground;

import lib.Bitmap;
import lib.BitmapBuilder;
import lib.BitmapGenerator;
import lib.bitmaps.Tile;
import lib.utilities.transformers.LeftCutter;
import lib.utilities.transformers.Overlay;
import lib.utilities.transformers.Rotater;

public class DirtGrass extends Tile{

    public DirtGrass(int w, int h) {
        super(w, h);
    }

    @Override
    public Bitmap generate() {
        
        Dirt dirt = new Dirt(getWidth(), getHeight());
        Bitmap base = dirt.generate();
        
        Grass grass = new Grass(getWidth(), getHeight());
        Bitmap top = grass.generate();
        
        BitmapBuilder generator = new BitmapBuilder();
        top = generator.modifyBitmap(top, new LeftCutter(0.7, 0.95), new Rotater(-1));
        
        base = generator.modifyBitmap(base, new Overlay(top, 1));
        
        return base;
    }

}
