package lib.bitmaps.minecraft.tiles.ground;

import java.awt.Color;

import lib.Bitmap;
import lib.BitmapBuilder;
import lib.utilities.Generator;
import lib.utilities.Gradient;
import lib.utilities.filter.GradientColorizer;
import lib.utilities.generators.RandomNoise;

public class Grass extends Ground{

    public Grass(int w, int h) {
        super(w, h);
    }

    @Override
    public Bitmap generate() {
        BitmapBuilder builder = new BitmapBuilder();
        Generator generator = new RandomNoise(true);
        
        Bitmap grass = builder.modifyBitmap(generator.generate(getWidth(), getHeight()),
                new GradientColorizer(
                        new Gradient(20, 240, new Color(151, 198, 103), new Color(80, 144, 38)))); 
        
        return grass;
    }

    @Override
    protected Gradient getGradient() {
        return new Gradient(20, 240, new Color(151, 198, 103), new Color(80, 144, 38));
    }

}
