package lib.bitmaps.minecraft.tiles.ground;

import java.awt.Color;

import lib.utilities.Gradient;

public class Gravel extends Ground{

    public Gravel(int w, int h) {
        super(w, h);
    }

    @Override
    protected Gradient getGradient() {
        return new Gradient(10, 250,
                new Color(179, 176, 175),
                new Color(161, 139, 133),
                new Color(85, 79, 77));
    }

}
