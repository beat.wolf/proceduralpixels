package lib.bitmaps.minecraft.tiles.ground;

import lib.Bitmap;
import lib.BitmapBuilder;
import lib.bitmaps.Tile;
import lib.utilities.Generator;
import lib.utilities.Gradient;
import lib.utilities.filter.GradientColorizer;
import lib.utilities.generators.RandomNoise;

public abstract class Ground extends Tile{

    public Ground(int w, int h) {
        super(w, h);
    }
    
    @Override
    public Bitmap generate() {
        BitmapBuilder builder = new BitmapBuilder();
        Generator generator = new RandomNoise(true);
        
        Bitmap grass = builder.modifyBitmap(generator.generate(getWidth(), getHeight()),
                new GradientColorizer(getGradient())); 
        
        return grass;
    }

    protected abstract Gradient getGradient();
}
