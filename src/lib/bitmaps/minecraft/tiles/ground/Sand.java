package lib.bitmaps.minecraft.tiles.ground;

import java.awt.Color;

import lib.utilities.Gradient;

public class Sand extends Ground{

    public Sand(int w, int h) {
        super(w, h);
    }
    
    @Override
    protected Gradient getGradient() {
        return new Gradient(80, 255,
                new Color(251, 244, 189),
                new Color(191, 184, 129));
    }

}
