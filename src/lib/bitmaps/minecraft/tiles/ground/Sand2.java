package lib.bitmaps.minecraft.tiles.ground;

import lib.Bitmap;
import lib.utilities.filter.Blur;

public class Sand2 extends Sand{

    public Sand2(int w, int h) {
        super(w, h);
    }
    
    @Override
    public Bitmap generate() {
        Bitmap sand = super.generate();
        new Blur().transform(sand);
        
        return sand;
    }
}
