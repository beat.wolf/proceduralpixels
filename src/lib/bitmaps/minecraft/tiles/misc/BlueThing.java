package lib.bitmaps.minecraft.tiles.misc;

import java.awt.Color;

import lib.Bitmap;
import lib.BitmapBuilder;
import lib.bitmaps.Tile;
import lib.utilities.Generator;
import lib.utilities.Gradient;
import lib.utilities.filter.Blur;
import lib.utilities.filter.GradientColorizer;
import lib.utilities.generators.RandomNoise;

public class BlueThing extends Tile{

    public BlueThing(int w, int h) {
        super(w, h);
    }

    @Override
    public Bitmap generate() {
        Generator noiseGen = new RandomNoise(true);
        
        Bitmap bitmap = noiseGen.generate(getWidth(), getHeight());
        BitmapBuilder builder = new BitmapBuilder();
        
        bitmap = builder.modifyBitmap(bitmap, 
                new GradientColorizer(new Gradient(0, 255, 
                        new Color(0,0,255), new Color(0,0,150))),
                        new Blur());
        
        return bitmap;
    }
    
}
