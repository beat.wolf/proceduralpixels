package lib.bitmaps.minecraft.tiles.misc;

import java.awt.Color;

import lib.Bitmap;
import lib.BitmapBuilder;
import lib.beans.PColor;
import lib.bitmaps.Tile;
import lib.utilities.Generator;
import lib.utilities.Gradient;
import lib.utilities.filter.Colorizer;
import lib.utilities.filter.GradientColorizer;
import lib.utilities.generators.RandomNoise;
import lib.utilities.transformers.Overlay;
import lib.utilities.transformers.Scale;

public class Wool extends Tile{

    private PColor color;
    
    public Wool(int w, int h, PColor color) {
        super(w, h);
        this.color = color;
        if(color.getAlpha() != 0){
            color.setAlpha(170);
        }
    }
    
    public Wool(int w, int h) {
        this(w,h, PColor.createColor(0, 0, 0, 0));
    }
    
    @Override
    public Bitmap generate() {
        BitmapBuilder builder = new BitmapBuilder();
        Generator generator = new RandomNoise(true);
        
        Bitmap wool = builder.modifyBitmap(generator.generate(getWidth() / 2, getHeight()),
                new Overlay(generator.generate(getWidth(), getHeight()), 0.3),
                new GradientColorizer(new Gradient(20, 255, Color.white, new Color(220, 220, 220))),
                new Colorizer(color.getJavaColor(), false),
                new Scale(2, 1)
                );
        
        return wool;
    }
}
