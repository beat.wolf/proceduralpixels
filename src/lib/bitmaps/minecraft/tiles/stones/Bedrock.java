package lib.bitmaps.minecraft.tiles.stones;

import java.awt.Color;

import lib.Bitmap;
import lib.BitmapBuilder;
import lib.bitmaps.Tile;
import lib.utilities.Generator;
import lib.utilities.Gradient;
import lib.utilities.filter.GradientColorizer;
import lib.utilities.generators.LineNoise;

public class Bedrock extends Tile{

    public Bedrock(int w, int h) {
        super(w, h);
    }

    @Override
    public Bitmap generate() {
        Generator generator = new LineNoise(0.3);
        BitmapBuilder builder = new BitmapBuilder();
        
        Bitmap stone = builder.modifyBitmap(generator.generate(getWidth(), getHeight()),
                new GradientColorizer(
                        new Gradient(50, 255, new Color(151, 151, 151), new Color(7, 7, 7)))); 
        
        return stone;
    }
    
}
