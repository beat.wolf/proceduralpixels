package lib.bitmaps.minecraft.tiles.stones;

import java.awt.Color;

public class Bluestone extends SpecialStone{

    public Bluestone(int w, int h) {
        super(w, h);
    }

    @Override
    protected Color getLowColor() {
        return new Color(0,0,255);
    }

    @Override
    protected Color getHighColor() {
        return new Color(0,0,150);
    }

}
