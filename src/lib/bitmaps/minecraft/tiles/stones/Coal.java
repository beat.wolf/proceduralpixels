package lib.bitmaps.minecraft.tiles.stones;

import java.awt.Color;

public class Coal extends SpecialStone{

    public Coal(int w, int h) {
        super(w, h);
    }
    
    @Override
    protected Color getLowColor() {
        return new Color(69, 69, 69);
    }

    @Override
    protected Color getHighColor() {
        return new Color(52, 52, 52);
    }
}
