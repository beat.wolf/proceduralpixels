package lib.bitmaps.minecraft.tiles.stones;

import java.awt.Color;

public class Crystal extends SpecialStone{

    
    public Crystal(int w, int h) {
        super(w, h);
    }

    @Override
    protected Color getLowColor() {
        return new Color(93, 236, 245);
    }

    @Override
    protected Color getHighColor() {
        return new Color(255,255,255);
    }
}
