package lib.bitmaps.minecraft.tiles.stones;

import java.awt.Color;

public class Gold extends SpecialStone{

    public Gold(int w, int h) {
        super(w, h);
    }

    @Override
    protected Color getLowColor() {
        return new Color(255, 255, 181);
    }

    @Override
    protected Color getHighColor() {
        return new Color(248, 175, 43);
    }

    
    
}
