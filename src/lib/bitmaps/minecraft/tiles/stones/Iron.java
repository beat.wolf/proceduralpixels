package lib.bitmaps.minecraft.tiles.stones;

import java.awt.Color;

import lib.Bitmap;

public class Iron extends SpecialStone{

    
    public Iron(int w, int h) {
        super(w, h);
    }

    @Override
    protected Color getLowColor() {
        return new Color(226, 192, 170);
    }

    @Override
    protected Color getHighColor() {
        return new Color(175, 142, 119);
    }
}
