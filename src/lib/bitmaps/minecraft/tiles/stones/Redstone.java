package lib.bitmaps.minecraft.tiles.stones;

import java.awt.Color;

public class Redstone extends SpecialStone{

    public Redstone(int w, int h) {
        super(w, h);
    }

    @Override
    protected Color getLowColor() {
        return new Color(143, 3, 3);
    }

    @Override
    protected Color getHighColor() {
        return new Color(255, 0, 0);
    }
}
