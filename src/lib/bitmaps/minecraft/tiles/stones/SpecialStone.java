package lib.bitmaps.minecraft.tiles.stones;

import java.awt.Color;

import lib.Bitmap;
import lib.BitmapBuilder;
import lib.utilities.Generator;
import lib.utilities.Gradient;
import lib.utilities.filter.AbsoluteTransparency;
import lib.utilities.filter.BitmapCutter;
import lib.utilities.filter.Blur;
import lib.utilities.filter.ColorCutter;
import lib.utilities.filter.GradientColorizer;
import lib.utilities.generators.LineNoise;
import lib.utilities.generators.RandomNoise;
import lib.utilities.generators.geometry.LineGenerator;
import lib.utilities.transformers.Overlay;
import lib.utilities.transformers.Scale;

public abstract class SpecialStone extends Stone{

    public SpecialStone(int w, int h) {
        super(w, h);
    }
    
    @Override
    public Bitmap generate() {
        Bitmap stone = super.generate();
        
        Generator generator = new LineNoise(0.2);
        BitmapBuilder builder = new BitmapBuilder();
        
        Bitmap overlay = builder.modifyBitmap(generator.generate(getWidth(), getHeight()),
                //new Blur(),
                new GradientColorizer(
                        new Gradient(140, 230,
                                new Color(255, 255, 255 ,0),
                                new Color(0,0,0,255))),
                new Blur(),
                new AbsoluteTransparency(),
                new Overlay(new RandomNoise(true).generate(getWidth(), getHeight()), 0.8),
                new GradientColorizer(
                        new Gradient(100, 255,
                                getLowColor(),
                                getHighColor())),
        new BitmapCutter(new LineGenerator(0.2, 0).generate(getWidth(), getHeight()))
                );
        
        /*Bitmap overlay = builder.modifyBitmap(
                new RandomNoise(true).generate(getWidth()/2, getHeight()/2),
                new Scale(2),
                new Overlay(new RandomNoise(true).generate(getWidth(), getHeight()), 0.3),
                new Blur(),
                new ColorCutter(Color.black, new Color(180,180,180)),
                new Overlay(new RandomNoise(true).generate(getWidth(), getHeight()), 0.3),
                new GradientColorizer(
                        new Gradient(100, 255,
                                getLowColor(),
                                getHighColor())),
                new BitmapCutter(new LineGenerator(0.2, 0).generate(getWidth(), getHeight()))
                );*/
        
        return builder.modifyBitmap(stone, new Overlay(overlay, 1));
    }

    protected abstract Color getLowColor();
    protected abstract Color getHighColor();
}
