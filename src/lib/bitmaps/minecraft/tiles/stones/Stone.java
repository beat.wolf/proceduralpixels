package lib.bitmaps.minecraft.tiles.stones;

import java.awt.Color;

import lib.Bitmap;
import lib.BitmapBuilder;
import lib.bitmaps.Tile;
import lib.utilities.Generator;
import lib.utilities.Gradient;
import lib.utilities.filter.GradientColorizer;
import lib.utilities.generators.LineNoise;

public class Stone extends Tile{

    public Stone(int w, int h) {
        super(w, h);
    }

    @Override
    public Bitmap generate() {
        Generator generator = new LineNoise(0.2);
        BitmapBuilder builder = new BitmapBuilder();
        
        Bitmap stone = builder.modifyBitmap(generator.generate(getWidth(), getHeight()),
                new GradientColorizer(
                        new Gradient(20, 240, new Color(143, 143, 143), new Color(104, 104, 104)))); 
        
        return stone;
    }

}
