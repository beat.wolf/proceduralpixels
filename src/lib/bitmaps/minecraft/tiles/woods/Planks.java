package lib.bitmaps.minecraft.tiles.woods;

import java.awt.Color;

import lib.Bitmap;
import lib.BitmapBuilder;
import lib.bitmaps.Tile;
import lib.utilities.Generator;
import lib.utilities.Gradient;
import lib.utilities.filter.Colorizer;
import lib.utilities.filter.GradientColorizer;
import lib.utilities.filter.TransparencyInverter;
import lib.utilities.generators.LineNoise;
import lib.utilities.generators.RandomNoise;
import lib.utilities.generators.geometry.LineGenerator;
import lib.utilities.transformers.Overlay;

public class Planks extends Tile{

    public Planks(int w, int h) {
        super(w, h);
    }
    
    @Override
    public Bitmap generate() {
        Generator generator = new LineNoise(0.3);
        BitmapBuilder builder = new BitmapBuilder();
        
        Bitmap base = builder.modifyBitmap(generator.generate(getWidth(), getHeight()),
                new GradientColorizer(
                        new Gradient(50, 255, new Color(188, 152, 98), new Color(159, 132, 77)))); 
        
        Bitmap sprinkle = builder.modifyBitmap(
                new RandomNoise(true).generate(getWidth(), getHeight()),
                new GradientColorizer(
                        new Gradient(245, 245, new Color(0,0,0,0), new Color(115, 94, 57))));
        
        base = builder.modifyBitmap(base,
                new Overlay(sprinkle,1));
        
        Bitmap overlay = builder.modifyBitmap(
                new LineGenerator(0.2, 0).generate(getWidth(), getHeight()),
                new TransparencyInverter(),
                new Colorizer(new Color(85, 64, 31, 180), true));
        
        
        Bitmap plank = builder.modifyBitmap(base,
                new Overlay(overlay,1));
        
        return plank;
    }
}
