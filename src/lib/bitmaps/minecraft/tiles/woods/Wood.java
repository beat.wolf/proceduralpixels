package lib.bitmaps.minecraft.tiles.woods;

import java.awt.Color;

import lib.Bitmap;
import lib.BitmapBuilder;
import lib.bitmaps.Tile;
import lib.utilities.Generator;
import lib.utilities.Gradient;
import lib.utilities.filter.BitmapCutter;
import lib.utilities.filter.Colorizer;
import lib.utilities.filter.GradientColorizer;
import lib.utilities.generators.LineNoise;
import lib.utilities.generators.geometry.LineGenerator;
import lib.utilities.transformers.Overlay;
import lib.utilities.transformers.Rotater;

public class Wood extends Tile{

    
    public Wood(int w, int h) {
        super(w, h);
    }

    @Override
    public Bitmap generate() {
        Generator generator = new LineNoise(0.3);
        BitmapBuilder builder = new BitmapBuilder();
        
        Bitmap wood = builder.modifyBitmap(generator.generate(getWidth(), getHeight()),
                new Rotater(1),
                new GradientColorizer(
                        new Gradient(50, 255, new Color(155, 123, 76), new Color(57, 44, 25)))); 
        
        
        return wood;
    }
}
