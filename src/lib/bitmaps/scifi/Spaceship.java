package lib.bitmaps.scifi;

import java.awt.Color;

import lib.Bitmap;
import lib.BitmapBuilder;
import lib.BitmapGenerator;
import lib.beans.PColor;
import lib.utilities.Generator;
import lib.utilities.Gradient;
import lib.utilities.filter.Colorizer;
import lib.utilities.filter.ColorCutter;
import lib.utilities.filter.GradientColorizer;
import lib.utilities.filter.Support;
import lib.utilities.generators.RandomNoise;
import lib.utilities.generators.geometry.ColorGenerator;
import lib.utilities.transformers.LeftCutter;
import lib.utilities.transformers.MirrorHorizontal;
import lib.utilities.transformers.Overlay;
import lib.utilities.transformers.Rotater;
import lib.utilities.transformers.Scale;

public class Spaceship implements BitmapGenerator{

    private int w, h;
    private long seed;
    
    public Spaceship(int w, int h){
        this(System.currentTimeMillis(), w, h);
    }
    
    public Spaceship(long seed, int w, int h){
        this.w = w;
        this.h = h;
        this.seed = seed;
    }
    
    @Override
    public Bitmap generate() {
        BitmapBuilder builder = new BitmapBuilder();
        
        Generator generator = new RandomNoise(seed, true);
        
        Bitmap bitmapBase = builder.modifyBitmap(generator.generate(w / 4, h / 4), new Scale(4));
        
        Bitmap noise = generator.generate(bitmapBase.getWidth(), bitmapBase.getHeight());
        
        Bitmap texture = builder.modifyBitmap(bitmapBase,
                new Overlay(noise, 0.3),
                new GradientColorizer(new Gradient(0, 255, Color.red, Color.black, Color.orange)),
                new Rotater(1),
                new MirrorHorizontal(),
                new Rotater(-1));
        
        Bitmap ship = builder.modifyBitmap(
                new ColorGenerator(PColor.create()).generate(bitmapBase.getWidth(), bitmapBase.getHeight()),
                new LeftCutter(seed, 0, 0.4),
                new Rotater(1),
                new MirrorHorizontal(),
                new Rotater(-1),
                new Support(),
                new ColorCutter(Color.black, new Color(150,150,150)),
                new Overlay(texture, 1));
        
        return ship;
    }

}
