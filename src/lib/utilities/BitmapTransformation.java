package lib.utilities;

import lib.Bitmap;

public interface BitmapTransformation {
	
	public enum Axis{
		Horizontal,
		Vertical
	}

	public void transform(Bitmap bitmap);
	
}
