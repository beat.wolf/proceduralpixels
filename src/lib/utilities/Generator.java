package lib.utilities;

import lib.Bitmap;

public interface Generator {

    public Bitmap generate(int width, int height);
    
}
