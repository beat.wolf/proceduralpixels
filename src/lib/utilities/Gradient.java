package lib.utilities;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import lib.PixelTools;

public class Gradient {

	private int min;
	private int max;
	
	private List<Color> colors = new ArrayList<Color>();
	
	public Gradient(int min, int max, Color ... colors){
		for(Color color: colors){
			this.colors.add(color);
		}
		
		this.min = min;
		this.max = max;
	}
	
	private Color getLowestColor(){
		return colors.get(0);
	}
	
	private Color getHighestColor(){
		return colors.get(colors.size() - 1);
	}
	
	public Color getColorForValue(int value){
		if(value <= min){
			return getLowestColor();
		}else if(value >= max){
			return getHighestColor();
		}
		
		double pieceLength = (max - min) / (double) (colors.size() - 1);
		int lowerColor = (int)((value - min) / pieceLength);
		
		return getGradient(
				(int)(min + lowerColor*pieceLength), (int)(min + (lowerColor+1)*pieceLength),
				colors.get(lowerColor), colors.get(lowerColor+1),
				value);
	}
	
	private Color getGradient(int min, int max, Color low, Color high, int value){
		double percentage = (value - min) / (double)(max - min);
		
		//Color top = new Color(high.getRed(), high.getGreen(), high.getBlue(), (int)(percentage * 255));
		
		//return PixelTools.overlayColors(low, top);
		return PixelTools.mergeColors(low,high, 1 - percentage);
	}
}
