package lib.utilities.filter;

import java.awt.Color;

import lib.Bitmap;
import lib.utilities.BitmapTransformation;

public class AbsoluteTransparency implements BitmapTransformation{

    @Override
    public void transform(Bitmap bitmap) {
        for(int x = 0; x < bitmap.getWidth(); x++){
            for(int y = 0; y < bitmap.getHeight(); y++){
                Color temp = new Color(bitmap.pixels[x][y], true);
                
                if(temp.getAlpha() != 0 && temp.getAlpha() != 255){
                    int alpha = (int)(Math.round(temp.getAlpha() / 255.) * 255);
                    temp = new Color(temp.getRed(), temp.getGreen(), temp.getBlue(), alpha);
                }
                
                bitmap.pixels[x][y] = temp.getRGB();
            }
        }
    }

}
