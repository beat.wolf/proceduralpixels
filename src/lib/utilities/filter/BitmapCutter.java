package lib.utilities.filter;

import java.awt.Color;

import lib.Bitmap;
import lib.utilities.BitmapTransformation;

public class BitmapCutter implements BitmapTransformation{

    private Bitmap overlay;
    
    public BitmapCutter(Bitmap overlay){
        this.overlay = overlay;
    }
    
    @Override
    public void transform(Bitmap bitmap) {
        int transparent = new Color(0,0,0,0).getRGB();
        
        for(int x = 0; x < bitmap.getWidth(); x++){
            for(int y = 0; y < bitmap.getHeight(); y++){
                Color temp = new Color(overlay.pixels[x][y], true);
                
                if(temp.getAlpha() != 255){
                    bitmap.pixels[x][y] = transparent;
                }
            }
        }
    }

}
