package lib.utilities.filter;

import java.awt.Color;

import lib.Bitmap;
import lib.utilities.BitmapTransformation;

public class Blur implements BitmapTransformation{

    private Color getAverage(Bitmap bitmap, int x, int y){
        int r,g,b,a;
        r = g = b = a = 0;
        int pixels = 0;
        
        int myX = Math.max(0, x-1);
        for(; myX < bitmap.getWidth() && myX <= x+1 ; myX++){
            int myY = Math.max(0, y-1);
            for(; myY < bitmap.getHeight() && myY <= y+1; myY++){
                Color temp = new Color(bitmap.pixels[myX][myY], true);
                r += temp.getRed();
                g += temp.getGreen();
                b += temp.getBlue();
                a += temp.getAlpha();
                
                pixels++;
            }
        }
        
        r /= pixels;
        g /= pixels;
        b /= pixels;
        a /= pixels;
        
        return new Color(r,g,b,a);
    }
    
    @Override
    public void transform(Bitmap bitmap) {
        
        Bitmap workingCopy = new Bitmap(bitmap.getWidth(), bitmap.getHeight());
        
        for(int x = 0; x < bitmap.getWidth(); x++){
            for(int y = 0; y < bitmap.getHeight(); y++){
                workingCopy.pixels[x][y] = getAverage(bitmap, x, y).getRGB();
            }
        }
        
        bitmap.pixels = workingCopy.pixels;
    }
    
}
