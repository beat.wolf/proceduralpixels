package lib.utilities.filter;

import java.awt.Color;

import lib.Bitmap;
import lib.utilities.BitmapTransformation;

/**
 * Sets all pixels between min and max color to transparent
 * @author beat
 *
 */
public class ColorCutter implements BitmapTransformation{

    private Color min, max;
    
    public ColorCutter(Color min, Color max){
        this.min = min;
        this.max = max;
    }
    
    @Override
    public void transform(Bitmap bitmap) {
        int transparent = new Color(0,0,0,0).getRGB();
        
        for(int x = 0; x < bitmap.getWidth(); x++){
            for(int y = 0; y < bitmap.getHeight(); y++){
                Color temp = new Color(bitmap.pixels[x][y], true);
                if(temp.getAlpha() > 0){
                    if(temp.getRed() >= min.getRed() && temp.getRed() <= max.getRed() &&
                        temp.getGreen() >= min.getGreen() && temp.getGreen() <= max.getGreen() &&
                        temp.getBlue() >= min.getBlue() && temp.getBlue() <= max.getBlue()){
                        bitmap.pixels[x][y] = transparent;
                    }
                }
            }
        }
        
    }

}
