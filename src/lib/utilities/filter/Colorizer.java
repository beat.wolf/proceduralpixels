package lib.utilities.filter;

import java.awt.Color;

import lib.Bitmap;
import lib.PixelTools;
import lib.utilities.BitmapTransformation;

/**
 * Replaces (or colors) all existing colors of a bitmap
 * @author beat
 *
 */
public class Colorizer implements BitmapTransformation{

	private Color color;
	private boolean override;
	
	public Colorizer(Color color, boolean override){
		this.color = color;
		this.override = override;
	}
	
	@Override
	public void transform(Bitmap bitmap) {
		for(int x = 0; x < bitmap.getWidth(); x++){
			for(int y = 0; y < bitmap.getHeight(); y++){
			    Color base = new Color(bitmap.pixels[x][y], true);
				if(override){
				    if(base.getAlpha() > 0){
				        bitmap.pixels[x][y] = color.getRGB();
				    }
				}else{
				    bitmap.pixels[x][y] = PixelTools.overlayColors(base, color).getRGB();
				}
			}
		}
	}

	
	
}
