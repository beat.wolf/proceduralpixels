package lib.utilities.filter;

import java.awt.Color;

import lib.Bitmap;
import lib.utilities.BitmapTransformation;
import lib.utilities.Gradient;

public class GradientColorizer implements BitmapTransformation{

	private Gradient gradient;
	
	public GradientColorizer(Gradient gradient){
		this.gradient = gradient;
	}
	
	@Override
	public void transform(Bitmap bitmap) {
		for(int x = 0; x < bitmap.getWidth(); x++){
			for(int y = 0; y < bitmap.getHeight(); y++){
				Color base = new Color(bitmap.pixels[x][y], true);
				Color newColor = gradient.getColorForValue(base.getGreen());
				int alpha = (newColor.getAlpha() * base.getAlpha()) / 255;
				bitmap.pixels[x][y] = 
				        new Color(newColor.getRed(), newColor.getGreen(), newColor.getBlue(), alpha).getRGB();
			}
		}
	}

}
