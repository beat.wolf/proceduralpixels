package lib.utilities.filter;

import java.awt.Color;

import lib.Bitmap;
import lib.utilities.BitmapTransformation;

public class Support implements BitmapTransformation{

    private Color getSupportColor(Bitmap bitmap, int x, int y){
        int pixels = 0;
        int support = 0;
        
        Color base = new Color(bitmap.pixels[x][y], true);
        
        int myX = Math.max(0, x-1);
        for(; myX < bitmap.getWidth() && myX <= x+1 ; myX++){
            int myY = Math.max(0, y-1);
            for(; myY < bitmap.getHeight() && myY <= y+1; myY++){
                Color temp = new Color(bitmap.pixels[myX][myY], true);
                support += temp.getAlpha();
                pixels++;
            }
        }
        support /= pixels;
        
        return new Color(support,support,support,base.getAlpha());
    }
    
    @Override
    public void transform(Bitmap bitmap) {
        Bitmap workingCopy = new Bitmap(bitmap.getWidth(), bitmap.getHeight());
        
        for(int x = 0; x < bitmap.getWidth(); x++){
            for(int y = 0; y < bitmap.getHeight(); y++){
                workingCopy.pixels[x][y] = getSupportColor(bitmap, x, y).getRGB();
            }
        }
        
        bitmap.pixels = workingCopy.pixels;
    }

}
