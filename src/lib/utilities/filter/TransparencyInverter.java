package lib.utilities.filter;

import java.awt.Color;

import lib.Bitmap;
import lib.utilities.BitmapTransformation;

public class TransparencyInverter implements BitmapTransformation{

    @Override
    public void transform(Bitmap bitmap) {
        for(int x = 0; x < bitmap.getWidth(); x++){
            for(int y = 0; y < bitmap.getHeight(); y++){
                Color temp = new Color(bitmap.pixels[x][y], true);
                
                bitmap.pixels[x][y] = new Color(temp.getRed(),
                        temp.getGreen(),
                        temp.getBlue(),
                        255 - temp.getAlpha()).getRGB();
            }
        }
    }

}
