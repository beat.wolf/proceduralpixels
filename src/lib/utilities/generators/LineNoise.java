package lib.utilities.generators;

import java.awt.Color;
import java.util.Random;

import lib.Bitmap;
import lib.PixelTools;
import lib.utilities.Generator;

public class LineNoise implements Generator{
    
    private long seed;
    private double maxLineLength;
    
    public LineNoise(double maxLength){
        this(System.currentTimeMillis(), maxLength);
    }
    
    public LineNoise(long seed, double maxLength){
        this.maxLineLength = maxLength;
        this.seed = seed;
    }

    @Override
    public Bitmap generate(int width, int height) {
        Bitmap bitmap = new Bitmap(width, height);
        Random rand = new Random(seed);
        
        int maxLength = (int)(width*maxLineLength);
        for(int y = 0; y < height; y++){
            int x = 0;
            while(x < width){
                int length = 1 + rand.nextInt(maxLength);
                
                int color = rand.nextInt(256);
                Color randomColor = new Color(color,color,color);
                
                PixelTools.fillRect(bitmap, x, y, length, 1, randomColor);
                x+= length;
            }
        }
        
        return bitmap;
    }

}
