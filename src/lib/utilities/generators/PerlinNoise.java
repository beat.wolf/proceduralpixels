package lib.utilities.generators;

import java.awt.Color;

import lib.Bitmap;
import lib.utilities.BitmapTransformation;
import lib.utilities.Generator;

public class PerlinNoise implements Generator{

	private Color perlin2d(int x, int y){
		return Color.black;
	}

    @Override
    public Bitmap generate(int width, int height) {
        Bitmap bitmap = new Bitmap(width, height);
        for(int x = 0; x < bitmap.getWidth(); x++){
            for(int y = 0; y < bitmap.getHeight(); y++){
                bitmap.pixels[x][y] = perlin2d(x, y).getRGB();
            }
        }
        return bitmap;
    }
	
}
