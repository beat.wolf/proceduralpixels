package lib.utilities.generators;

import java.awt.Color;
import java.util.Random;

import lib.Bitmap;
import lib.utilities.Generator;

public class RandomNoise implements Generator{

	private boolean blackAndWhite;
	private long seed;
	
	public RandomNoise(boolean blackAndWhite){
		this(System.currentTimeMillis(), blackAndWhite);
	}
	
	public RandomNoise(long seed, boolean blackAndWhite){
		this.blackAndWhite = blackAndWhite;
		this.seed = seed;
	}

    @Override
    public Bitmap generate(int width, int height) {
        Bitmap bitmap = new Bitmap(width, height);
        Random rand = new Random(seed);
        
        for(int x = 0; x < bitmap.getWidth(); x++){
            for(int y = 0; y < bitmap.getHeight(); y++){
                Color randomColor;
                if(blackAndWhite){
                    int color = rand.nextInt(256);
                    randomColor = new Color(color,color,color);
                    
                }else{
                    randomColor = new Color(rand.nextInt(256), rand.nextInt(256), rand.nextInt(256));
                }
                bitmap.pixels[x][y] = randomColor.getRGB();
            }
        }
        return bitmap;
    }

}
