package lib.utilities.generators.geometry;

import java.awt.Color;

import lib.Bitmap;
import lib.PixelTools;
import lib.utilities.Generator;

public class CircleGenerator implements Generator{

    private double radius;
    
    public CircleGenerator(double radius){
        this.radius = radius;
    }
    
    @Override
    public Bitmap generate(int width, int height) {
        Bitmap bitmap = new Bitmap(width, height);
        PixelTools.fillRect(bitmap, 0, 0, width, height, new Color(0,0,0,0));
        
        double centerX = width / 2.;
        double centerY = height / 2.;
        
        double wRadius = Math.pow(radius * width / 2, 2);
        double hRadius = Math.pow(radius * height / 2, 2);
        
        for(int x = 0; x < width; x++) {
        	for(int y = 0; y < height; y++) {
        		
        		double a = Math.pow(centerX -x, 2) / wRadius;
        		double b = Math.pow(centerY - y, 2) / hRadius;
        		
        		if(a + b <= 1) {
        			bitmap.pixels[x][y] = Color.black.getRGB();
        		}
        	}
        }
        
        return bitmap;
    }

}
