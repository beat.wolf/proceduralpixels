package lib.utilities.generators.geometry;

import java.awt.Color;

import lib.Bitmap;
import lib.beans.PColor;
import lib.utilities.Generator;

public class ColorGenerator implements Generator{

    private PColor color;
    
    public ColorGenerator(Color color){
    	this.color = PColor.createColor(color);
    }
    
    public ColorGenerator(PColor color){
        this.color = color;
    }
    
    @Override
    public Bitmap generate(int width, int height) {
        Bitmap bitmap = new Bitmap(width, height);
        int c = color.getRGBA();
        
        for(int x = 0; x < bitmap.getWidth(); x++){
            for(int y = 0; y < bitmap.getHeight(); y++){
                bitmap.pixels[x][y] = c;
            }
        }
        
        return bitmap;
    }

}
