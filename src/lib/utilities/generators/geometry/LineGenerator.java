package lib.utilities.generators.geometry;

import java.awt.Color;

import lib.Bitmap;
import lib.PixelTools;
import lib.utilities.Generator;

public class LineGenerator implements Generator{

    private double lineSize;
    private double lineDistance;
    
    public LineGenerator(double lineSize, double lineDistance){
        this.lineSize = lineSize;
        this.lineDistance = lineDistance;
    }
    
    @Override
    public Bitmap generate(int width, int height) {
        Bitmap bitmap = new Bitmap(width, height);
        PixelTools.fillRect(bitmap, 0, 0, width, height, new Color(0,0,0,0));
        
        int lineHeight = (int)Math.max(1, height * lineSize);
        int lineGap = (int)Math.max(1, height * lineDistance);
        int y = 0;
        while(y < height){
            PixelTools.fillRect(bitmap, 0, y, width, lineHeight, Color.black);
            y += lineHeight + lineGap;
        }
        
        return bitmap;
    }

}
