package lib.utilities.pictures;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import lib.Bitmap;
import lib.PixelTools;

public class PNGLoader {

    public Bitmap loadImage(File imageFile) throws IOException{
        BufferedImage image = ImageIO.read(imageFile);
        
        return PixelTools.createBitmapFromBuffer(image);
    }
    
}
