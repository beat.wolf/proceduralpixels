package lib.utilities.random;

public class RandomNumber {

	public static double getNumber(double min, double max) {
		return min + Math.random() * (max - min);
	}
	
	public static int getRandomIndex(int min, int max) {
		return (int)(min + Math.random() * (max - min));
	}
}
