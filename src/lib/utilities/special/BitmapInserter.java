package lib.utilities.special;

import java.awt.Color;

import lib.Bitmap;
import lib.PixelTools;

public class BitmapInserter {
    
    private boolean replace;
    
    public BitmapInserter(boolean replace){
        this.replace = replace;
    }

    public Bitmap insert(Bitmap base, Bitmap overlay, int xOffset, int yOffset){
        
        for(int x = 0; x < overlay.getWidth() && x + xOffset < base.getWidth(); x++){
            for(int y = 0; y < overlay.getHeight() && y + yOffset < base.getHeight(); y++){
                Color overlayColor = new Color(overlay.pixels[x][y], true);
                
                Color newColor;
                if(replace){
                    newColor = overlayColor;
                }else{
                    Color baseColor = new Color(base.pixels[xOffset+x][yOffset+y], true);
                    newColor = PixelTools.overlayColors(baseColor, overlayColor);
                }
                
                base.pixels[xOffset+x][yOffset+y] = newColor.getRGB();
                        
            }
        }
        
        return base;
    }
    
}
