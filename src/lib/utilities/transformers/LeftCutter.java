package lib.utilities.transformers;

import java.awt.Color;
import java.util.Random;

import lib.Bitmap;
import lib.utilities.BitmapTransformation;

public class LeftCutter implements BitmapTransformation{
	
	private long seed;
	private double leftMax;
	private double leftMin;
	private double minSegmentLength;
	
	public LeftCutter(double leftMin, double leftMax){
		this(System.currentTimeMillis(), leftMin, leftMax);
	}
	
	public LeftCutter(long seed, double leftMin, double leftMax){
		this.seed = seed;
		this.leftMax = leftMax;
		this.leftMin = leftMin;
		this.minSegmentLength = minSegmentLength;
	}
	
	@Override
	public void transform(Bitmap bitmap) {
		Random rand = new Random(seed);
		
		int color = new Color(0, 0, 0, 0).getRGB();
		
		int left = (int)(bitmap.getWidth() * leftMin);
		int maxLength = (int)((leftMax - leftMin) * bitmap.getWidth());
		
		int depth = 0;
		
	   for(int y = 0; y < bitmap.getHeight(); y++){
	        depth = left + (int)(maxLength * rand.nextDouble());
		    
			for(int x = 0; x < depth; x++){
				bitmap.pixels[x][y] = color;
			}
		}
	}

}
