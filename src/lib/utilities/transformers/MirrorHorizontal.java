package lib.utilities.transformers;

import lib.Bitmap;
import lib.utilities.BitmapTransformation;

public class MirrorHorizontal implements BitmapTransformation {
	
	public MirrorHorizontal(){
	}
	
	@Override
	public void transform(Bitmap bitmap) {
		int boundaryA;
		
		boundaryA = bitmap.getHeight() / 2;
		if(bitmap.getHeight() % 2 == 1){
			boundaryA--;
		}
		
		for(int loop = 0; loop < bitmap.getWidth(); loop++){
			
			int secondPosition = bitmap.getHeight() - 1;
			
			for(int mirrorLoop = 0; mirrorLoop < boundaryA; mirrorLoop++){
				int x, y;
				
				x = loop;
				y = mirrorLoop;
				
				bitmap.pixels[x][secondPosition - y] = bitmap.pixels[x][y];
			}
		}
	}

	
	
}
