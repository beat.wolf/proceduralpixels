package lib.utilities.transformers;

import lib.Bitmap;
import lib.utilities.BitmapTransformation;
import lib.utilities.special.BitmapInserter;

public class Overlay implements BitmapTransformation{

	private final Bitmap overlay;
	private final boolean center;
	
	public Overlay(Bitmap overlay, double alpha, boolean center){
		this.overlay = overlay.clone();
		this.center = center;
		Tranparent trans = new Tranparent(alpha);
		trans.transform(this.overlay);
	}
	
	public Overlay(Bitmap overlay, double alpha){
	    this(overlay, alpha, false);
	}
		
	@Override
	public void transform(Bitmap bitmap) {
	    BitmapInserter inserter = new BitmapInserter(false);
	    
	    int x = 0;
	    int y = 0;
	    
	    if(center) {
	    	x = (bitmap.getWidth() - overlay.getWidth()) / 2;
	    	y = (bitmap.getHeight() - overlay.getHeight()) / 2;
	    }
	    
	    inserter.insert(bitmap, overlay, x, y);
	}

}
