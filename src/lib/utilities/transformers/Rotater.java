package lib.utilities.transformers;

import lib.Bitmap;
import lib.utilities.BitmapTransformation;

public class Rotater implements BitmapTransformation{

	private int ticks;
	
	/**
	 * One tick is 90� clockwise, negative ticks allowed
	 * @param ticks
	 */
	public Rotater(int ticks){
		this.ticks = ticks;
	}
	
	@Override
	public void transform(Bitmap bitmap) {
		
		ticks = ticks % 4;
		if(ticks < 0){
			ticks = 4 + ticks;
		}
		
		for(int i = 0; i < ticks; i++){
			bitmap = rotateOneTick(bitmap);
		}
	}
	
	private Bitmap rotateOneTick(Bitmap bitmap){
		Bitmap newBitmap = new Bitmap(bitmap.getHeight(),bitmap.getWidth());
		
		for(int y = 0; y < newBitmap.getHeight(); y++){
			for(int x = 0; x < newBitmap.getWidth(); x++){
				int x2 = y;
				int y2 = newBitmap.getWidth() - 1 - x;
				
				newBitmap.pixels[x][y] = bitmap.pixels[x2][y2];
			}
		}
		
		bitmap.pixels = newBitmap.pixels;
		return bitmap;
	}
}
