package lib.utilities.transformers;

import lib.Bitmap;
import lib.utilities.BitmapTransformation;

public class Scale implements BitmapTransformation{

	private int multiplierX;
	private int multiplierY;
	
	public Scale(int multiplier){
		this(multiplier, multiplier);
	}
	
	public Scale(int multiplierX, int multiplierY){
        this.multiplierX = multiplierX;
        this.multiplierY = multiplierY;
    }
	
	@Override
	public void transform(Bitmap bitmap) {
		
		int [][] newPixels = new int[bitmap.getWidth()*multiplierX][bitmap.getHeight()*multiplierY];
		
		for(int x = 0; x < bitmap.getWidth(); x++){
			for(int y = 0; y < bitmap.getHeight(); y++){
				
				for(int x2 = 0; x2 < multiplierX; x2++){
					for(int y2 = 0; y2 < multiplierY; y2++){
						newPixels[x*multiplierX+x2][y*multiplierY+y2] = bitmap.pixels[x][y];
					}
				}
			}
		}
		
		bitmap.pixels = newPixels;
	}

}
