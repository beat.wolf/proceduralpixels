package lib.utilities.transformers;

import java.awt.Color;

import lib.Bitmap;
import lib.utilities.BitmapTransformation;

public class Tranparent implements BitmapTransformation{

    private double alpha;
    
    public Tranparent(double alpha){
        this.alpha = alpha;
    }
    
    @Override
    public void transform(Bitmap bitmap) {
        for(int x = 0; x < bitmap.getWidth(); x++){
            for(int y = 0; y < bitmap.getHeight(); y++){
                Color base = new Color(bitmap.pixels[x][y], true);
                int a = (int)( base.getAlpha() * alpha);
                
                Color newColor = new Color(base.getRed(), base.getGreen(), base.getBlue(), a);
                bitmap.pixels[x][y] = newColor.getRGB();
            }
        }
    }

}
