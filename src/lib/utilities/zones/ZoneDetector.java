package lib.utilities.zones;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import lib.Bitmap;
import lib.beans.ColoredRectangle;
import lib.beans.PColor;
import lib.beans.Point;

public class ZoneDetector {

	public static List<ColoredRectangle> getZones(Bitmap source, List<PColor> zoneColors){
		
		Point [] minPoints = new Point[zoneColors.size()];
		Point [] maxPoints = new Point[zoneColors.size()];
		
		for(int i = 0; i < minPoints.length; i++){
			minPoints[i] = new Point(Integer.MAX_VALUE, Integer.MAX_VALUE);
			maxPoints[i] = new Point(Integer.MIN_VALUE, Integer.MIN_VALUE);
		}
		
		for(int x = 0; x < source.getWidth(); x++){
			for(int y = 0; y < source.getHeight(); y ++){
				int pixel = source.pixels[x][y];
				
				for(int i = 0; i < zoneColors.size(); i++){
					if(pixel == zoneColors.get(i).getRGBA()){
						minPoints[i].setX(Math.min(minPoints[i].getX(), x));
						minPoints[i].setY(Math.min(minPoints[i].getY(), y));
						
						maxPoints[i].setX(Math.max(maxPoints[i].getX(), x));
						maxPoints[i].setY(Math.max(maxPoints[i].getY(), y));
					}
				}
			}
		}
		
		List<ColoredRectangle> zones = new ArrayList<ColoredRectangle>();
		
		for(int i = 0; i < zoneColors.size(); i++){
			if(minPoints[i].getX() != Integer.MAX_VALUE && minPoints[i].getY() != Integer.MAX_VALUE){
				ColoredRectangle zone = new ColoredRectangle(minPoints[i], maxPoints[i], zoneColors.get(i));			
				zones.add(zone);
			}
		}
		
		return zones;
	}
	
}
