package tests;

import static org.junit.Assert.*;

import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;

import org.junit.Test;

import lib.Bitmap;
import lib.beans.PColor;
import lib.utilities.pictures.PNGLoader;
import lib.utilities.zones.ZoneDetector;

public class TestBitmapZones {

	@Test
	public void testZonesRetrieval() throws IOException{
		PNGLoader loader = new PNGLoader();
		Bitmap bitmap = loader.loadImage(new File("data/archeology/location.png"));
		
		assertEquals(0, ZoneDetector.getZones(bitmap, Collections.EMPTY_LIST).size());
		
		PColor colorA = PColor.createColor(255, 0, 0);
		
		assertEquals(1, ZoneDetector.getZones(bitmap, Arrays.asList(colorA)).size());
		
		PColor colorB = PColor.createColor(new Color(0x01ff2b));
		
		assertEquals(1, ZoneDetector.getZones(bitmap, Arrays.asList(colorB)).size());
		

		assertEquals(2, ZoneDetector.getZones(bitmap, Arrays.asList(colorA, colorB)).size());
		
		PColor colorC = PColor.createColor(new Color(0x12345));
		assertEquals(0, ZoneDetector.getZones(bitmap, Arrays.asList(colorC)).size());
	}
	
}
