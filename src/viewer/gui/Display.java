package viewer.gui;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.awt.image.WritableRaster;

import javax.swing.text.html.ImageView;

import lib.Bitmap;
import lib.PixelTools;
import lib.utilities.transformers.Scale;

public class Display extends Canvas{

	private Bitmap bitmap;
	private int scale = 1;
	
	public Display(int scale){
	    this.scale = scale;
	}
	
	@Override
	public void paint(Graphics g) {
		if(bitmap == null){
			super.paint(g);
			return;
		}
		
		BufferedImage buffer = PixelTools.createImageBuffer(bitmap);
		g.drawImage(buffer, 0, 0, null);
	}

	@Override
	public int getHeight() {
		return bitmap.getHeight();
	}

	@Override
	public Dimension getSize() {
		return new Dimension(getWidth(), getHeight());
	}

	@Override
	public int getWidth() {
		return bitmap.getWidth();
	}

	public void setBitmap(Bitmap bitmap){
	    Scale scaler = new Scale(scale);
        Bitmap copy = bitmap.clone();
        scaler.transform(copy);
        
		this.bitmap = copy;
		repaint();
	}
	
	public int getScale(){
	    return scale;
	}
}
