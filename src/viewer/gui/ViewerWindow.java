package viewer.gui;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.imageio.ImageIO;
import javax.sound.sampled.Line;
import javax.swing.JFrame;

import lib.Bitmap;
import lib.BitmapBuilder;
import lib.PixelTools;
import lib.beans.ColoredRectangle;
import lib.beans.PColor;
import lib.beans.Point;
import lib.beans.Rectangle;
import lib.bitmaps.archeology.Coin;
import lib.bitmaps.heightmap.BasicHeightmap;
import lib.bitmaps.minecraft.MinecraftTiles;
import lib.bitmaps.minecraft.tiles.ground.Dirt;
import lib.bitmaps.minecraft.tiles.ground.DirtGrass;
import lib.bitmaps.minecraft.tiles.ground.Grass;
import lib.bitmaps.minecraft.tiles.ground.Gravel;
import lib.bitmaps.minecraft.tiles.ground.Sand;
import lib.bitmaps.minecraft.tiles.stones.Gold;
import lib.bitmaps.minecraft.tiles.stones.Stone;
import lib.bitmaps.scifi.Spaceship;
import lib.utilities.BitmapTransformation;
import lib.utilities.Gradient;
import lib.utilities.filter.Blur;
import lib.utilities.filter.Colorizer;
import lib.utilities.filter.GradientColorizer;
import lib.utilities.generators.RandomNoise;
import lib.utilities.generators.geometry.LineGenerator;
import lib.utilities.pictures.PNGLoader;
import lib.utilities.random.RandomNumber;
import lib.utilities.special.BitmapInserter;
import lib.utilities.transformers.LeftCutter;
import lib.utilities.transformers.MirrorHorizontal;
import lib.utilities.transformers.Overlay;
import lib.utilities.transformers.Rotater;
import lib.utilities.transformers.Scale;
import lib.utilities.zones.ZoneDetector;

public class ViewerWindow extends JFrame implements MouseListener{ 

	private Display display;
	private Bitmap bitmap;
	private File outputFile = new File("C:\\Users\\Asraniel\\Desktop\\image.png");
	private int scale = 3;
	
	public ViewerWindow(){
		bitmap = getBitmap();
		
		setLayout(new BorderLayout());
		display = new Display(scale);
        add(display, BorderLayout.CENTER);
        display.setBitmap(bitmap);
        
        setSize(bitmap.getWidth()*2 *display.getScale()+ 50, bitmap.getHeight()*2*display.getScale() + 50);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        display.addMouseListener(this);
	}
	
	private Bitmap getBitmap(){
		return getTest();
	}
	
	private Bitmap createMinecraftTile(){
	    //Grass tile = new Grass(16, 16);
	    //DirtGrass tile = new DirtGrass(100, 100);
	    //Stone tile = new Stone(16, 16);
	    //Sand tile = new Sand(16, 16);
	    //Gravel tile = new Gravel(16, 16);
	    MinecraftTiles tile = new MinecraftTiles(64, 64);
	    //Gold tile = new Gold(100,100);
	    return tile.generate();
	}
	
	private Bitmap getCoin(){
		Coin coin = new Coin(100, 100);
		return coin.generate();
	}
	
	private Bitmap getTest() {
		PNGLoader loader = new PNGLoader();
	    
	    try {
	        Bitmap base = PixelTools.resizeBitmap(loader.loadImage(new File("data/archeology/test.png")), 100, 100);
            
	        Bitmap overlay = PixelTools.resizeBitmap(loader.loadImage(new File("data/archeology/overlay.png")), 50, 50);
            
            Bitmap bitmap = loader.loadImage(new File("data/archeology/location.png"));
            List<ColoredRectangle> zones = ZoneDetector.getZones(bitmap, Arrays.asList(PColor.createColor(255, 0, 0), PColor.createColor(new Color(0x01ff2b))));
            
            int zoneIndex = RandomNumber.getRandomIndex(0, zones.size());
	        System.out.println("Zone "+zoneIndex);
	        
	        BitmapInserter inserter = new BitmapInserter(false);
	        Point center = zones.get(zoneIndex).getCenter();
	        return inserter.insert(base, overlay, center.getX(), center.getY());
	        
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
	    
	    return null;
	}
	
	private Bitmap getHeightmap(){
	    return new BasicHeightmap(100, 100).generate();
	}
	
	private Bitmap debug(){
		BitmapBuilder generator = new BitmapBuilder();
		
		Bitmap bitmapBase = generator.modifyBitmap(
				new LineGenerator(0,0).generate(100, 100));
		
		return bitmapBase;
	}
	
	private Bitmap createGrassTile(){		
		BitmapBuilder generator = new BitmapBuilder();
		
		Bitmap bitmapBase = generator.modifyBitmap(new RandomNoise(true).generate(16,16), new Scale(2));
		
		Bitmap noise = new RandomNoise(true).generate(bitmapBase.getWidth(), bitmapBase.getHeight());
		
		Bitmap grass = generator.modifyBitmap(bitmapBase, new Overlay(noise, 0.3),
				new Colorizer(new Color(0,255,0,100), false), new Scale(4));		
		return grass;
	}
	
	private Bitmap createSpaceShip(){
		Spaceship spaceShip = new Spaceship(16, 16);
		return spaceShip.generate();
	}

	private void outputPng(){
		BufferedImage image = PixelTools.createImageBuffer(bitmap);
		
		try {
		    ImageIO.write(image, "png", outputFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void mouseClicked(MouseEvent arg0) {
		bitmap = getBitmap();
		display.setBitmap(bitmap);
		outputPng();
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}
